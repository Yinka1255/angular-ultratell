import { Component, OnInit } from '@angular/core';
import { Http, Headers }   from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'; 
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppRoutingModule }  from '../app-routing.module';
import 'rxjs/add/operator/map';


import { UserService } from '../services/user.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  firstname: String;
  surname: String;
  email: String;
  passport: any;
  resume: any;
  letter: String;
  fd:any = new FormData();
  url1: String;
  url2: String;


   

  constructor(private http: Http, private route: ActivatedRoute, private router: Router) { 
  
  }

  ngOnInit() {
    
  }

  

  showMessage(title, text, type){
    swal({
      title: title,
      text: text,
      type: type,
      confirmButtonText: 'Ok'
    })
  }
}
