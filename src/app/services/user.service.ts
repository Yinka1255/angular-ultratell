import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import {AppSettings} from '../appSettings';
 
@Injectable()
export class UserService{

  api : string;
    
  constructor(public http: Http) {
      this.http = http;
      this.api = AppSettings.API_ENDPOINT;
  }

  ngOnInit() {
  }

  

  login(user){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api+'users/login', JSON.stringify(user), {headers: headers}) 
                .map(res => res.json());

  }         

}
