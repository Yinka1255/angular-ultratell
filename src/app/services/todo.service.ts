import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import {AppSettings} from '../appSettings';
 
@Injectable()
export class TodoService{

  api : string;
    
  constructor(public http: Http) {
      this.http = http;
      this.api = AppSettings.API_ENDPOINT;
  }

  ngOnInit() {
  }

  
  createNew(data){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api+'todos/create_new', JSON.stringify(data), {headers: headers}) 
                .map(res => res.json());
                
  }     
  
  updateTodo(data){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.api+'todos/update', JSON.stringify(data), {headers: headers}) 
                .map(res => res.json());
                
  }     

  getTodos(){
    return this.http.get(this.api+'todos/get')
                .map(res => res.json());
  }

  deleteTodo(todoId){
    return this.http.get(this.api+'todos/delete/'+todoId)
                .map(res => res.json());
  }

  completeTodo(todoId){
    return this.http.get(this.api+'todos/complete/'+todoId)
                .map(res => res.json());
  }

}
