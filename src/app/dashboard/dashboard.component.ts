import { Component, OnInit } from '@angular/core';
import { Http, Headers }   from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'; 
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppRoutingModule }  from '../app-routing.module';
import "rxjs/add/operator/map";
import {AppSettings} from '../appSettings';

import swal from 'sweetalert2';
declare var jquery:any;
declare var $ :any;
declare var require :any;

import { UserService } from '../services/user.service';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loggedInUser: any;
  todos: any;
  api: String;
  title: string;
  description: string;
  editId: any;
  editTitle: String;
  editDescription: String;
  editCompletionDate: any;
  completionDate: Date = new Date();
  
  constructor(private http: Http, private todoService: TodoService, private route: ActivatedRoute, private router: Router) { 
    this.api = AppSettings.API_ENDPOINT;
    this.checkIfLoggedIn();
    this.getTodos();
  }

  ngOnInit() {
    
  }

  createNew(){
    var data = {'title': this.title, 'description': this.description, 'completionDate': this.completionDate};
    this.todoService.createNew(data).subscribe(response => {
      console.log(response);
      if (response.response =="error"){
        console.log(response);
        this.showMessage("Error", "Sorry! An error occured", 'error'); 
       }else{
          this.getTodos();
          $(".login-modal-lg").modal("hide");
          this.showMessage("Success", "Todo item saved successfully", 'success'); 
      }
    });        
  }

  updateTodo(){
    var data = {'_id': this.editId, 'title': this.editTitle, 'description': this.editDescription, 'completionDate': this.editCompletionDate};
    this.todoService.updateTodo(data).subscribe(response => {
      console.log(response);
      if (response.response =="error"){
        console.log(response);
        this.showMessage("Error", "Sorry! An error occured", 'error'); 
       }else{
          this.getTodos();
          $(".edit-modal-lg").modal("hide");
          this.showMessage("Success", "Todo item updated successfully", 'success'); 
      }
    });        
  }

  delete(todoId){
    this.todoService.deleteTodo(todoId).subscribe(response => {
      this.getTodos();
      this.showMessage("Success", "Todo item deleted successfully", 'success'); 
    });
  }

  complete(todoId){
    this.todoService.completeTodo(todoId).subscribe(response => {
      this.getTodos();
      this.showMessage("Success", "Todo item has been completed", 'success'); 
    });
  }

  getTodos(){
    this.todoService.getTodos().subscribe(response => {
      this.todos = response.todos; 
      console.log(response);
    });
  }
  

  checkIfLoggedIn(){
    if (window.localStorage.getItem('loggedInUser')){
        this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
        
    }else
    {
      this.router.navigate(['login']);
    }  
  }

  showMessage(title, text, type){
    swal({
      title: title,
      text: text,
      type: type,
      confirmButtonText: 'Ok'
    })
  }

  closeSwal(){
    swal.close();
  }
  confirmDelete(todoId){
          swal({
      title: 'Confirm',
      text: "Are you sure you want to delete this item. This action cannot be reversed?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Delete',
      cancelButtonText: 'Cancel',
      confirmButtonClass: 'btn btn-danger',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
         this.delete(todoId);
      } 
      else{
        this.closeSwal();
      }
    })
  }

  edit(id, title, description, date){
    this.editId = id;
    this.editTitle = title;
    this.editDescription = description;
    this.editCompletionDate = date;
    $(".edit-modal-lg").modal("toggle");
  }

  confirmCompletion(todoId){
    swal({
      title: 'Confirm',
      text: "Are you sure you want to mark this item as complete?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0f0',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Complete',
      cancelButtonText: 'Cancel',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
      }).then((result) => {
      if (result.value) {
        this.complete(todoId);
      } 
      else{
        this.closeSwal();
      }
    })
  }
}
