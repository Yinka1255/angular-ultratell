import { Component } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'; 
import { AppRoutingModule }  from './app-routing.module';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	loggedIn : String;
	loggedInUser: any; 
	template: String ='<img src="http://pa1.narvii.com/5722/2c617cd9674417d272084884b61e4bb7dd5f0b15_hq.gif" />'

	constructor(private router: Router) { 
	this.checkIfLoggedIn();
	}

	title = 'app';

	checkIfLoggedIn(){
		if (window.localStorage.getItem('loggedInUser') && window.localStorage.getItem('appCode') == 'Ultratell'){
			this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
			this.router.navigate(['dashboard']);
		}else
		{
		  this.router.navigate(['index']);
		}  
	  }
	
	


}
