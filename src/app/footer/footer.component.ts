import { Component, OnInit, ElementRef, AfterContentInit } from '@angular/core';

import {AppSettings} from '../appSettings';

declare var jquery:any;
declare var $ :any;	

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  loggedInUser: any;
  api: String;
  term: any;


  constructor(private elementRef:ElementRef) { 
    this.api = AppSettings.API_ENDPOINT;

  }

  ngOnInit() {
    
  
  }

  
  ngAfterViewInit(){
	
  }

}
