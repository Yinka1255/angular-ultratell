import { Component, OnInit, ElementRef, AfterContentInit } from '@angular/core';

import {AppSettings} from '../appSettings';
import { Router } from '@angular/router'; 

declare var jquery:any;
declare var $ :any;	

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loggedInUser: any;
  api: String;
  term: any;


  constructor(private elementRef:ElementRef, private router: Router) { 
    this.api = AppSettings.API_ENDPOINT;
    this.checkIfLoggedIn();
  }

  ngOnInit() {
    
  
  }

  checkIfLoggedIn(){
		if (window.localStorage.getItem('loggedInUser') && window.localStorage.getItem('appCode') == 'Ultratell'){
			this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
		}
  }
    
  ngAfterViewInit(){
	
  }

  logout(){
    window.localStorage.clear();
    location.href = '/index';
  }

}
