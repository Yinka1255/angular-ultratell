import { Component, OnInit } from '@angular/core';
import { Http, Headers }   from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router'; 
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AppRoutingModule }  from '../app-routing.module';
import "rxjs/add/operator/map";


import { UserService } from '../services/user.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	email : string;
	password : string;

  constructor(private http: Http, private userService: UserService, private route: ActivatedRoute, private router: Router) { 
    //document.getElementById('ggg').style.display = 'none';

  }

  ngOnInit() {
  }

  login(){
    var user = {'username': this.email, 'password': this.password};
    this.userService.login(user).subscribe(response => {
      console.log(response);
      if (response.response =="error"){
        console.log(response);
        this.showMessage("Error", "Invalid login details", 'error'); 
       }else{
          localStorage.setItem('loggedInUser', JSON.stringify(response.user));
          localStorage.setItem('appCode', 'Ultratell');
          location.href = 'dashboard';
      }
    });        
  }

  showMessage(title, text, type){
    swal({
      title: title,
      text: text,
      type: type,
      confirmButtonText: 'Ok'
    })
  }


}
